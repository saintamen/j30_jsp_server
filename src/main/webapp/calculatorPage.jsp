<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 12/8/19
  Time: 1:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> <%--fmt ... ... --%>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Calculator</title>
</head>
<body>
    <form action="${pageContext.request.contextPath}/calculator" method="post">
        Liczba pierwsza: <input type="number" step="0.01" name="pierwsza"> <br/>
        Liczba druga: <input type="number" step="0.01" name="druga"> <br/>
        Działanie:
        <select name="dzialanie">
            <option value="+" selected>+</option>
            <option value="*">*</option>
            <option value="/">/</option>
            <option value="-">-</option>
        </select> <br/> <br/>

        <input type="submit"/>
    </form>
<hr> <%--linia horyzontalna--%>

<c:if test="${requestScope.odpowiedz != null}">
    Odpowiedź: ${requestScope.odpowiedz}
</c:if>
</body>
</html>
