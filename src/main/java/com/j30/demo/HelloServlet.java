package com.j30.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/index")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String imie = req.getParameter("name");

        // służy do przekazywania zmiennych z klas servletowych do widoku (jsp)
        req.setAttribute("imie", imie);

        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
