package com.j30.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

@WebServlet("/calculator")
public class CalculatorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/calculatorPage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String liczbaPierwsza = req.getParameter("pierwsza");
        String liczbaDruga = req.getParameter("druga");
        String dzialanie = req.getParameter("dzialanie");

        try{
            Double pierwsza = Double.parseDouble(liczbaPierwsza);
            Double druga = Double.parseDouble(liczbaDruga);

            Double wynik = null;
            switch (dzialanie){
                case "+":
                    wynik = pierwsza+druga;
                    break;
                case "-":
                    wynik = pierwsza-druga;
                    break;
                case "/":
                    wynik = pierwsza/druga;
                    break;
                case "*":
                    wynik = pierwsza*druga;
                    break;
                default:
//                    error
                    wynik= null;
            }
            req.setAttribute("odpowiedz", wynik);

        }catch (NumberFormatException nfe){
            req.setAttribute("odpowiedz", "Błąd zią! Podałeś złe parametry!");
        }
        req.getRequestDispatcher("/calculatorPage.jsp").forward(req, resp);
    }
}
